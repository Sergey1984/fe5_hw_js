function CreateTable(cols, rows) {
    let body = document.getElementsByTagName('body')[0]
    // console.log(body);
    let table = document.createElement('table');
    table.className = 'table';
    for (let i = 0; i < rows; i++) {
        let tableRow = document.createElement('tr');
        tableRow.className = "rows";
        for (let j = 0; j < cols; j++) {
            let tableColumn = document.createElement('td')
            tableColumn.className = 'column';
            tableRow.appendChild(tableColumn)
        }
        table.appendChild(tableRow)
    }
    document.body.appendChild(table)
    let changeColor = function (e) {
        if( e.target.tagName === "TD"){
            e.target.classList.toggle("column-active")
        } else if(e.target === this) {
            let tdTable = document.getElementsByClassName('column')
            for (let i = 0; i<tdTable.length; i++){
                tdTable[i].classList.toggle("column-active")
            }
        }
    }

    body.addEventListener('click',changeColor);

}

CreateTable(30, 30)

