// Техническое задание:

// Функция-конструктор должна выглядеть следующим образом:
// class Hamburger {
//    ...
// }
//
//
// Необходимо использовать геттеры-сеттеры и прочие возможности классов в ES6.


class Hamburger {
    constructor(size, stuffing) {
        try {
            try {
                if (!size) {
                    throw new HamburgerException("No size")
                } else if (!stuffing) {
                    throw new HamburgerException("No stuffing")
                }

                let flag = false;
                for (let key in Hamburger) {
                    if (key.toLowerCase().indexOf("size") !== -1 && size === Hamburger[key]) {
                        flag = true;
                    }
                }
                if (!flag) {
                    throw new HamburgerException("wrong size")
                }

                flag = false;
                for (let key in Hamburger) {
                    if (key.toLowerCase().indexOf("stuffing") !== -1 && stuffing === Hamburger[key]) {
                        flag = true;
                    }
                }
                if (!flag) {
                    throw new HamburgerException("wrong staffing")
                }

                let burgerSize = size;
                this.getSize = function () {
                    return burgerSize
                };
                this.setSize = function (value) {
                    return burgerSize = value
                };
                let burgerStuffing = stuffing;
                this.getStuffing = function () {
                    return burgerStuffing
                };
                this.setStuffing = function (value) {
                    return burgerStuffing = value
                };

                let burgerToppings = [];
                this.getBurgerToppings = function () {
                    return burgerToppings;
                }
                this.setBurgerToppings = function (value) {
                    return burgerToppings = value
                }
            } catch (e) {
                console.log("Hamburger exeption:" + e.message);
            }
        } catch (e) {
            console.log(e.message);
        }

    }

    addTopping(topping) {
        try {
            if (this.getBurgerToppings().includes(topping)) {
                throw new HamburgerException("duplicate topping")
            }

            if (!this.getBurgerToppings().includes(topping)) {
                return this.getBurgerToppings().push(topping)
            }

        } catch (e) {
            console.log("Hamburger exeption:" + e.message);
        }
    }

    removeTopping(topping) {

        try {
            if (!this.getBurgerToppings().includes(topping)) {
                throw new HamburgerException("no topping remove not possible")
            }
            if (this.getBurgerToppings().includes(topping)) {
                let filteredToppings = this.getBurgerToppings().filter(function (toppingType) {
                    return toppingType !== topping
                })
                return this.setBurgerToppings(filteredToppings)
            }
        } catch (e) {
            console.log("Hamburger exeption:" + e.message);
        }
    }

    calculatePrice() {
        return (this.getBurgerToppings().map(function (x) {
                return x.price
            })).reduce(function (acc, prices) {
                return acc + prices
            }, 0)
            + this.getSize().price + this.getStuffing().price

    }

    calculateCallories() {

        return (this.getBurgerToppings().map(function (x) {
                return x.calories
            })).reduce(function (acc, prices) {
                return acc + prices
            }, 0)
            + this.getSize().calories + this.getStuffing().calories

    }


}

function HamburgerException(message) {
    this.message = message;
}

Hamburger.SIZE_SMALL = {
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    price: 100,
    calories: 40
};
Hamburger.STUFFING_CHEESE = {
    price: 10,
    calories: 20
};
Hamburger.STUFFING_SALAD = {
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POTATO = {
    price: 15,
    calories: 10
};

Hamburger.ALL_TOPPINGS = {
    TOPPING_MAYO: {
        name: 'TOPPING_MAYO',
        price: 20,
        calories: 5
    },
    TOPPING_SPICE: {
        name: 'TOPPING_SPICE',
        price: 15,
        calories: 0
    }
};


let test = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);


// test.addTopping(Hamburger.ALL_TOPPINGS.TOPPING_MAY);
// test.addTopping(Hamburger.ALL_TOPPINGS.TOPPING_MAYO);
// test.addTopping(Hamburger.ALL_TOPPINGS.TOPPING_MAYO);
// test.addTopping(Hamburger.ALL_TOPPINGS.TOPPING_SPICE);
// test.addTopping(Hamburger.ALL_TOPPINGS.TOPPING_SPICE);
// console.log(test.getToppings());
// test.removeTopping(Hamburger.ALL_TOPPINGS.TOPPING_MAYO)
// test.removeTopping(Hamburger.ALL_TOPPINGS.TOPPING_MAYO)
// test.removeTopping(Hamburger.ALL_TOPPINGS.TOPPING_SPICE)
// test.removeTopping(Hamburger.ALL_TOPPINGS.TOPPING_SPICE)
// console.log(test.getToppings());
console.log(test.calculateCallories());
console.log(test.calculatePrice());
// console.log(test.getSize());
